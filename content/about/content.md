A site developed by Kegan and Tim.

```
How we deployed our site:

Picked this theme: https://www.gatsbyjs.org/starters/fabe/gatsby-universal/

Then followed these guides:

https://www.gatsbyjs.org/tutorial/part-zero/

https://www.gatsbyjs.org/tutorial/part-one/

Then edited our site to customize it for ourselves.

https://www.gatsbyjs.org/docs/deploying-to-gitlab-pages/

To use images in markdown

https://spectrum.chat/gatsby-js/general/gatsby-v2-embed-images-in-markdown~e8406ffb-d9af-4731-8625-1ab0f2f8b95d
and
https://github.com/gatsbyjs/gatsby/issues/2389
```
