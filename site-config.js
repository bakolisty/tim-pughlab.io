const path = require('path');

module.exports = {
  siteTitle: `Oregon Mountain Report Live`,
  siteTitleShort: `OMRL`,
  siteDescription: `All your mountain information in one place.`,
  siteUrl: `https://gu.fabianschultz.com`,
  themeColor: `#000`,
  backgroundColor: `#fff`,
  pathPrefix: null,
  logo: path.resolve(__dirname, 'src/images/icon.png'),
  social: {
    twitter: `gatsbyjs`,
    fbAppId: `966242223397117`,
  },
};
