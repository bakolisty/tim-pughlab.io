import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Img from 'gatsby-image';
import { Title, Copy } from './item.css';
import axios from "axios"

class Item extends Component {
  state = {
    loading: false,
    error: false,
    weather: {
      summary: '', curTemp: '', maxTemp: '', minTemp: ''
    },
  }

  componentDidMount() {
    this.fetchWeather()
  }

  render() {

    const { curTemp, maxTemp, minTemp } = this.state.weather

    return (
      <figure>
        <Img fluid={this.props.image ? this.props.image.childImageSharp.fluid : {}} alt={this.props.title} />
        <figcaption>
          <Title>{this.props.title}</Title>
          <Copy>
            {this.state.loading ? (
              <p>Please hold, data incoming!</p>
            ) : curTemp ? (
              <>
                <h2>Current Temperature: {curTemp}&deg;F</h2>
                <h2>High: {maxTemp}&deg;F</h2>
                <h2>Low: {minTemp}&deg;F</h2>
              </>
            ) : (
                  <p>Oh no, error fetching data :(</p>
                )}
          </Copy>
        </figcaption>
      </figure>
    )
  }

  // This data is fetched at run time on the client.
  fetchWeather = () => {
    this.setState({ loading: true })

    if(this.props.title === "Mt. Hood Meadows Weather Report:") {
      axios
      .get(`https://api.openweathermap.org/data/2.5/weather?id=5741683&units=imperial&APPID=10208b55077ea683c540a40df9b2ae0f`)
      .then(weather => {
        const {
          data: { main: summary },
        } = weather
        const curTemp = summary['temp']
        const maxTemp = summary['temp_max']
        const minTemp = summary['temp_min']

        this.setState({
          loading: false,
          weather: {
            ...this.state.weather,
            curTemp, maxTemp, minTemp,
          },
        })
      })
      .catch(error => {
        this.setState({ loading: false, error })
      })
    }

    else if(this.props.title === "SkiBowl Weather Report:") {
      axios
      .get(`https://api.openweathermap.org/data/2.5/weather?lat=45.3017&lon=-121.7725&units=imperial&APPID=10208b55077ea683c540a40df9b2ae0f`)
      .then(weather => {
        const {
          data: { main: summary },
        } = weather
        const curTemp = summary['temp']
        const maxTemp = summary['temp_max']
        const minTemp = summary['temp_min']

        this.setState({
          loading: false,
          weather: {
            ...this.state.weather,
            curTemp, maxTemp, minTemp,
          },
        })
      })
      .catch(error => {
        this.setState({ loading: false, error })
      })
    }

    else if(this.props.title === "Mt. Bachelor Weather Report:") {
      axios
      .get(`https://api.openweathermap.org/data/2.5/weather?lat=43.9792&lon=-121.6886&units=imperial&APPID=10208b55077ea683c540a40df9b2ae0f`)
      .then(weather => {
        const {
          data: { main: summary },
        } = weather
        const curTemp = summary['temp']
        const maxTemp = summary['temp_max']
        const minTemp = summary['temp_min']

        this.setState({
          loading: false,
          weather: {
            ...this.state.weather,
            curTemp, maxTemp, minTemp,
          },
        })
      })
      .catch(error => {
        this.setState({ loading: false, error })
      })
    }
  }

}

Item.propTypes = {
  title: PropTypes.string,
  image: PropTypes.object.isRequired,
};

export default Item
