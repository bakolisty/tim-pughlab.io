import styled from 'styled-components';

export const Container = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 4rem;

  a {
    color: #cccccc;
    transition: color 0.2s ease;
    text-decoration: none;

    &:hover {
      color: #db0808;
    }
  }
`;
